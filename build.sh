#!/bin/bash
set -o xtrace

if [ ! -d trial ]
then
    git clone https://github.com/gedaiu/trial.git
    cd trial
else
    cd trial
    git pull https://github.com/gedaiu/trial.git -f
fi

dub run :runner -- --coverage
dub build :lifecycle -b ddox
ls
ls .trial
cd ..

mv trial/.trial artifacts
mv trial/coverage artifacts/coverage
mv trial/docs api

mv artifacts/allure artifacts/allure-raw
rm api/styles/ddox.css
mv assets/css/ddox.css api/styles/ddox.css
allure -v generate --output artifacts/allure artifacts/allure-raw

pwd
ls -lsa
