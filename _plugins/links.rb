Jekyll::Hooks.register :pages, :post_render do |post|
  newContent = post.content.gsub(/\.md/,'.html')      # do something you want onto conetent....
  newContent = newContent.gsub(/\/README.html/,'/')      # do something you want onto conetent....
  post.content = newContent
  if post.place_in_layout?
    site = post.site
    payload = site.site_payload
    content = post.content
    info = {
      :filters   => [Jekyll::Filters],
      :registers => { :site => site, :page => payload['page'] }
    }
    post.output = Jekyll::Renderer.new(site, post, payload).place_in_layouts(content, payload, info)
  end
end
